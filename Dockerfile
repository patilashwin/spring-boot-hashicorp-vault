FROM openjdk:8-jre
VOLUME /tmp
WORKDIR /data
COPY target/spring-boot-hashicorp-vault-0.0.1-SNAPSHOT.jar  /data
EXPOSE 8090
ENTRYPOINT ["java" ,"-jar", "spring-boot-hashicorp-vault-0.0.1-SNAPSHOT.jar"]
