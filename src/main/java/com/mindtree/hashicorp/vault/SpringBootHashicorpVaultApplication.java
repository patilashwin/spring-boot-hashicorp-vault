package com.mindtree.hashicorp.vault;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHashicorpVaultApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHashicorpVaultApplication.class, args);
	}

}
