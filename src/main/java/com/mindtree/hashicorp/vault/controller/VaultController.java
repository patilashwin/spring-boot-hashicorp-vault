package com.mindtree.hashicorp.vault.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.hashicorp.vault.config.PropertyConfiguration;

@RestController
@RequestMapping("/vault")
public class VaultController {

	@Autowired
	PropertyConfiguration config;
	
	@Autowired
	VaultTemplate template;

	@GetMapping
	public String getCredentials() {
		String plainText = template.opsForTransit().decrypt("spring-boot-hashicorp-vault", config.getPassword());
		return String.format("Username: %s  Password: %s and Country: %s ", config.getUsername(), plainText, config.getCountries());
	}

}
